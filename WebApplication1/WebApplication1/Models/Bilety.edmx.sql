
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/16/2018 00:48:54
-- Generated from EDMX file: C:\Users\Przemek\Desktop\bilety\bilety\WebApplication1\WebApplication1\Models\Bilety.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [System Rezerwacji Biletów];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Bilety_Miejsca]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Bilety] DROP CONSTRAINT [FK_Bilety_Miejsca];
GO
IF OBJECT_ID(N'[dbo].[FK_Bilety_Wydarzenia]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Bilety] DROP CONSTRAINT [FK_Bilety_Wydarzenia];
GO
IF OBJECT_ID(N'[dbo].[FK_Dane_Klienta_Bilety]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dane_Klienta] DROP CONSTRAINT [FK_Dane_Klienta_Bilety];
GO
IF OBJECT_ID(N'[dbo].[FK_Dane_Klienta_Rabaty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Dane_Klienta] DROP CONSTRAINT [FK_Dane_Klienta_Rabaty];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Bilety]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bilety];
GO
IF OBJECT_ID(N'[dbo].[Dane_Klienta]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Dane_Klienta];
GO
IF OBJECT_ID(N'[dbo].[Miejsca]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Miejsca];
GO
IF OBJECT_ID(N'[dbo].[Rabaty]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rabaty];
GO
IF OBJECT_ID(N'[dbo].[Wydarzenia]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Wydarzenia];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Bilety'
CREATE TABLE [dbo].[Bilety] (
    [ID_Biletu] nchar(10)  NOT NULL,
    [Typ_Biletu] nchar(10)  NOT NULL,
    [Cena_Biletu] int  NOT NULL,
    [id_miejsca] nchar(10)  NULL,
    [id_wydarzenia] int  NULL
);
GO

-- Creating table 'Dane_Klienta'
CREATE TABLE [dbo].[Dane_Klienta] (
    [IDKlienta] int IDENTITY(1,1) NOT NULL,
    [Imie] nvarchar(max)  NOT NULL,
    [Nazwisko] nvarchar(max)  NOT NULL,
    [Rocznik] int  NOT NULL,
    [E_mail] nchar(10)  NOT NULL,
    [Telefon] nchar(10)  NULL,
    [id_biletu] nchar(10)  NULL,
    [id_rabatu] nchar(10)  NULL
);
GO

-- Creating table 'Miejsca'
CREATE TABLE [dbo].[Miejsca] (
    [ID_Miejsca] nchar(10)  NOT NULL,
    [Numer_Miejsca] nchar(10)  NOT NULL,
    [Sektor_Miejsca] nchar(10)  NOT NULL
);
GO

-- Creating table 'Rabaty'
CREATE TABLE [dbo].[Rabaty] (
    [ID_Rabatu] nchar(10)  NOT NULL,
    [Typ_Rabatu] nchar(10)  NOT NULL,
    [Znizka] int  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Wydarzenia'
CREATE TABLE [dbo].[Wydarzenia] (
    [ID_Wydarzenia] int IDENTITY(1,1) NOT NULL,
    [Nazwa_Wydarzenia] nchar(10)  NOT NULL,
    [Termin_Wydarzenia] nchar(10)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID_Biletu] in table 'Bilety'
ALTER TABLE [dbo].[Bilety]
ADD CONSTRAINT [PK_Bilety]
    PRIMARY KEY CLUSTERED ([ID_Biletu] ASC);
GO

-- Creating primary key on [IDKlienta] in table 'Dane_Klienta'
ALTER TABLE [dbo].[Dane_Klienta]
ADD CONSTRAINT [PK_Dane_Klienta]
    PRIMARY KEY CLUSTERED ([IDKlienta] ASC);
GO

-- Creating primary key on [ID_Miejsca] in table 'Miejsca'
ALTER TABLE [dbo].[Miejsca]
ADD CONSTRAINT [PK_Miejsca]
    PRIMARY KEY CLUSTERED ([ID_Miejsca] ASC);
GO

-- Creating primary key on [ID_Rabatu] in table 'Rabaty'
ALTER TABLE [dbo].[Rabaty]
ADD CONSTRAINT [PK_Rabaty]
    PRIMARY KEY CLUSTERED ([ID_Rabatu] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [ID_Wydarzenia] in table 'Wydarzenia'
ALTER TABLE [dbo].[Wydarzenia]
ADD CONSTRAINT [PK_Wydarzenia]
    PRIMARY KEY CLUSTERED ([ID_Wydarzenia] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [id_miejsca] in table 'Bilety'
ALTER TABLE [dbo].[Bilety]
ADD CONSTRAINT [FK_Bilety_Miejsca]
    FOREIGN KEY ([id_miejsca])
    REFERENCES [dbo].[Miejsca]
        ([ID_Miejsca])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Bilety_Miejsca'
CREATE INDEX [IX_FK_Bilety_Miejsca]
ON [dbo].[Bilety]
    ([id_miejsca]);
GO

-- Creating foreign key on [id_wydarzenia] in table 'Bilety'
ALTER TABLE [dbo].[Bilety]
ADD CONSTRAINT [FK_Bilety_Wydarzenia]
    FOREIGN KEY ([id_wydarzenia])
    REFERENCES [dbo].[Wydarzenia]
        ([ID_Wydarzenia])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Bilety_Wydarzenia'
CREATE INDEX [IX_FK_Bilety_Wydarzenia]
ON [dbo].[Bilety]
    ([id_wydarzenia]);
GO

-- Creating foreign key on [id_biletu] in table 'Dane_Klienta'
ALTER TABLE [dbo].[Dane_Klienta]
ADD CONSTRAINT [FK_Dane_Klienta_Bilety]
    FOREIGN KEY ([id_biletu])
    REFERENCES [dbo].[Bilety]
        ([ID_Biletu])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Dane_Klienta_Bilety'
CREATE INDEX [IX_FK_Dane_Klienta_Bilety]
ON [dbo].[Dane_Klienta]
    ([id_biletu]);
GO

-- Creating foreign key on [id_rabatu] in table 'Dane_Klienta'
ALTER TABLE [dbo].[Dane_Klienta]
ADD CONSTRAINT [FK_Dane_Klienta_Rabaty]
    FOREIGN KEY ([id_rabatu])
    REFERENCES [dbo].[Rabaty]
        ([ID_Rabatu])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Dane_Klienta_Rabaty'
CREATE INDEX [IX_FK_Dane_Klienta_Rabaty]
ON [dbo].[Dane_Klienta]
    ([id_rabatu]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------