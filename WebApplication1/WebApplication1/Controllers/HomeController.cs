﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            System_Rezerwacji_BiletówEntities db = new System_Rezerwacji_BiletówEntities();
            var wydarzenia = db.Wydarzenia;

            /*
            string connectionString = "Data Source = SAMSUNG_ULTRA; Initial Catalog = \"System Rezerwacji Biletów\"; Integrated Security = True";
            string query = "Select Typ_Biletu, Cena_Biletu from dbo.Bilety where ID_Biletu=@id";
            int id = 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@id", id);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                var typ = reader[0];
                var cena = reader[1];
                
                ViewBag.Typ = typ;
                ViewBag.Cena = cena;
               


            }
            */
            return View(wydarzenia);
        } 

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}