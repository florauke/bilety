﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class FormController : Controller
    {
        private System_Rezerwacji_BiletówEntities db = new System_Rezerwacji_BiletówEntities();

        // GET: Wydarzenias
        public ActionResult Index()
        {
            return View(db.Wydarzenia.ToList());
        }

        // GET: Wydarzenias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wydarzenia wydarzenia = db.Wydarzenia.Find(id);
            if (wydarzenia == null)
            {
                return HttpNotFound();
            }
            return View(wydarzenia);
        }

        // GET: Wydarzenias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Wydarzenias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Wydarzenia,Nazwa_Wydarzenia,Termin_Wydarzenia")] Wydarzenia wydarzenia)
        {
            if (ModelState.IsValid)
            {
                db.Wydarzenia.Add(wydarzenia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(wydarzenia);
        }

        // GET: Wydarzenias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wydarzenia wydarzenia = db.Wydarzenia.Find(id);
            if (wydarzenia == null)
            {
                return HttpNotFound();
            }
            return View(wydarzenia);
        }

        // POST: Wydarzenias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Wydarzenia,Nazwa_Wydarzenia,Termin_Wydarzenia")] Wydarzenia wydarzenia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wydarzenia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(wydarzenia);
        }

        // GET: Wydarzenias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wydarzenia wydarzenia = db.Wydarzenia.Find(id);
            if (wydarzenia == null)
            {
                return HttpNotFound();
            }
            return View(wydarzenia);
        }

        // POST: Wydarzenias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Wydarzenia wydarzenia = db.Wydarzenia.Find(id);
            db.Wydarzenia.Remove(wydarzenia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}